ALUSUS PROGRAMMING LANGUAGE
===========================

This folder contains Alusus' language specifications, documentation, and
source code.


COPYRIGHT
=========

Copyright (C) 2019.
The copyright for this project is owned by Sarmad Khalid Abdullah.


LICENSE
=======

Alusus Language specification, documentation, source code, binaries, and
examples are published under Alusus Public License, Version 1.0, which is
included in this directory and is also available online at
<http://alusus.net/alusus_license_1_0>. Please read the license before using
or copying this software. By using this software you acknowledge that you
have read the terms contained in this license and agree with and accept all
such terms.

Alusus Public License is designed to make the language open source while
protecting the language from fragmenting into multiple incompatible languages
at an early stage. The license allows:
* Obtaining the language in source code or binary format free of charge and
  use it to write any program, whether commercial or not.
* Modify the language and use the modified version to write any program,
  whether commercial or not.
* Redistribute the unmodified version of the language in source code or binary
  format. The license prevents distributing a modified version of the language
  except with a permission from Alusus Software Ltd.

For more information about the license please contact the team at:
http://alusus.net/contact

NOTE: Files that are released under a different license than Alusus Public
      License mentions at the beginning its own licensing information.


Directory Structure
===================

/Doc            Contains all the documentation of the project, including
                the language specifications, implementation documentation,
                and source code inline documentation.

/Sources        Contains the entire source code.
/Sources/Core   Contains the source code of the Core.
/Sources/Spp    Contains the source code of the Standard Programming Paradigm
                library.
/Sources/Srt    Contains the source for the Standard Runtime Library.
/Sources/Tests  Contains automated test projects for Core and the standard
                libraries.

/Notices_L18n   Contains localizations for build notices.

/Examples       Contains examples written in Alusus.

/Tools          Contains various development helper scripts.


File Name Language Extension
============================

File names (and folder names) can have an extension referring to the language
of the file. Extension ar refers to Arabic while extension en refers to English.

